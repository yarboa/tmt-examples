#!/bin/bash

echo -ne "Testing the binary: ${BINARY}...\t"

if [[ -x "${BINARY}" ]]; then
  echo "PASS"
else
  echo "FAIL"
  exit 1
fi
