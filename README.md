# tmt examples

This repository contains a few examples of how to use `tmt`. **tmt** is a tool
for managing tests.

Things that tmt can do for us:

* Add metadata to the tests.
* Define a workflow and order for the test to be run.
* Filter tests to be run by using tags and other means.
* Run remote tests.
* Run tests in a fresh and controled environment (container, virtual
machine...)
* Have different kind of reports for the tests and be able to ask for
the reports later.
* Run any kind of tests and test frameworks

More info here: <https://tmt.readthedocs.io>

We'll use `tmt` to define define and structure of the tests that will run
later in the **Testing Farm**.

**Testing Farm** is a Red Hat service for running tests in the cloud.
We'll see more about how it works and how to run the tests later.

Here is the API documentation: <https://testing-farm.gitlab.io/api/>



## Installation

If you're in **Fedora**, you can install it from the official repositories:

```shell
sudo dnf install -y tmt
```

It'll install the basic tool, but if you want to try using containers,
virtual machine or other ways to provision the testing environment, it'll
be better if you install the package `tmt-all`.

```shell
sudo dnf install -y tmt-all
```

In case you're in **RHEL 8**, **CentOS 8** or **CentOS Stream 8**, you'll need first to install and enable the EPEL
repository. For RHEL 8.X:

```shell
sudo dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
sudo dnf config-manager --set-enabled epel
sudo dnf install -y tmt-all
```

### Virtual Machines

If you installed the package `tmt-all` and want to use the `provision
--how virtual` to run the tests inside a virtual machine (VM), make sure
`libvirtd` is running, and that you're in the `libvirt` group.

```shell
sudo systemctl start libvirtd
sudo usermod -a -G libvirt $USER
```

> NOTE: More info about this point [here](https://tmt.readthedocs.io/en/stable/questions.html#virtualization-tips).

## tmt basics

`tmt` is a tool for managing the tests. It uses plain text files in [fmf](https://fmf.readthedocs.io/)
format to define the metadata for those tests.

We can define 3 levels of metadata:

* Tests
* Plans
* Stories
### Tests

Tests are the actual tests we want to run, plus some minimal metadata, so we
can locate, group, list, and filter them.

`tmt` doesn't care about the testing framework or language. You just define the
metadata around the tests and it'll run the tests.

### Plans

The plan is kind of a **CI pipeline**, where you define where you get the tests,
in what order they're going to run, any environment variables you need for the
tests, and any task you need to run before or after the tests.

### Stories

It's a place to define what we want to test and to keep track of the progress.

In our examples, we have this story:

> As a developer I want to be sure that a binary exists and
> it has executable permissions.

That would be the feature that we want to implement and test. And we can more
information as:

* Examples of what it looks like.
* Where the code lives.
* Where the tests live.
* Any ticket or bug related to the Story.
* And more.

## How to run the tests

The `tmt` command line has many options, but they're well documented. Try:

```shell
tmt --help
```

You'll see that you've got a few commands available: `run`, `init`, `tests`,
etc. You can run the `help` for each one of those commands to get more info:

```shell
tmt run --help
```

Then, again, you'll see more (sub-)commands like `provision`, `report`,
`login`, etc. And again, you can get the help for each one of those
sub-commands:

```shell
tmt run provision --help
tmt run report --help
tmt tests create --help
```

You can run more than one sub-command with the main command, but you need to
be careful with the order of the parameters and options. This is the schema:

```shell
tmt [command] [command or global options] [sub-command 1] [sub-command 1 options] ...
```

Example:

```shell
tmt run --all -v provision --how local
```

Where:

* `run` is the main command.
* `-v` is a global option.
* `--all` is an option for the command `run`.
* `provision` is the sub-command.
* `--how local` is an option for the sub-command `provision`.

More complex example:

```shell
tmt run -a -vv \
        provision -h connect --guest myserver --key ~/.ssh/id_rsa \
        plans --name /tests/all_pass
```

There you can see two sub-command (`provision` and `plans`) with their options.

### New tests

In order to `tmt` to know that your repo contains any `fmf` metadata, tests,
plans or stories, the root directory may be initialized with:

```shell
tmt init
```

It'll create a directory and file with the `fmf` version:

```shell
$ ls .fmf/
version
$ cat .fmf/version
1
```

> NOTE: All the examples here have already their directories initialized, so
> there is no need for doing it there.

## Examples

There is a directory for each example. All the examples try to test the same,
so it's easier to spot the differences. They are incremental, from the simplest
version possible to different degrees of complexity.

As you can see in the examples, you can structure the tests and plans as you
want. As long as you define them with the `fmf` files, `tmt` will find the
test. And it runs them as you specify in the plans.

### Tests

* [Basic test](basic_test)
* [Basic test using scripts](basic_test_scripts)

Here are some commands to try at those directories:

```shell
tmt tests ls
tmt tests show
tmt tests show -v
tmt run -a provision -h local
tmt run -a -v provision -h local
tmt run -a -vvv provision -h local
tmt run -a -vvv provision -h container
```

### Plans

* [Basic plan](basic_plan)
* [Basic plan using discover and execute](basic_plan_discover_execute)
* [Basic plan using discover and fmf](basic_plan_discover_fmf)
* [Basic plan using discover and shell](basic_plan_discover_shell)
* [Basic plan using Environment variables](basic_plan_environment)
* [Basic plan using Environment vars from scripts](basic_plan_environment_script)
* [Basic plan using tags](basic_plan_tags)
* [Plan using most of the features](full_plan)

Here are some commands to try at those directories:

```shell
tmt plans ls
tmt plans show
tmt plans show -v
tmt run -a provision -h local
tmt run -a -v provision -h local
tmt run -a -vvv provision -h local
tmt run -a -vvv provision -h container
```

### Stories

* [Basic Story](basic_story)

Here are some commands to try at that directory:

```shell
tmt stories ls
tmt stories show
tmt stories show -v
tmt stories coverage
tmt stories coverage --help
```

## Testing Farm

**Testing Farm** is a Red Hat service for running tests in the cloud.

Here is the API documentation: <https://testing-farm.gitlab.io/api/>

### Example of how to run tests in Testing Farm

```bash
ENDPOINT="https://api.dev.testing-farm.io/v0.1/requests"
API_KEY=${MY_SECRET_API_KEY}

cat <<EOF > request.json
{
  "api_key": "${API_KEY}",
  "test": {
    "fmf": {
      "url": "https://gitlab.com/redhat/edge/tests/base-image",
      "ref": "update-demo",
      "path": "/FFI",
      "name": "/containers"
    }
  },
  "environments": [
    {
      "arch": "x86_64",
      "os": {"compose": "CentOS-Stream-8"}
    }
  ]
}
EOF

curl --silent ${ENDPOINT} \
     --header "Content-Type: application/json" \
     --data @request.json
```

This will return a response with the `id` and more info. To check the status
of the tests, you can run:

```bash
curl --silent "${ENDPOINT}/${ID}"
```

Being the `${ID}` the id you got before in the response to your petition.
